let userNumber = Number(prompt('Enter a positive integer', ''));

let factorial = (participationNumbers) => {
    if (participationNumbers === 1) {
        return 1;
    } else {
        return participationNumbers * factorial(participationNumbers - 1);
    }
};
let resultNumber = factorial(userNumber);

document.write(`The factorial of ${userNumber} is ${resultNumber}.`);